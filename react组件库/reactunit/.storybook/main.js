/*
 * @Author: 李明明
 * @Date: 2020-08-17 10:03:52
 * @LastEditTime: 2020-08-17 16:41:36
 * @LastEditors: 李明明
 * @Description: 
 * @FilePath: /zhufeng-react-assembly/react组件库/reactunit/.storybook/main.js
 * @代码版权方：'政府采购信息网'
 */
module.exports = {
	"stories": ["../src/**/*.stories.tsx","../src/**/*.stories.(tsx|mdx)"],
	"addons": [
		"@storybook/preset-create-react-app",
		"@storybook/addon-actions",
		"@storybook/addon-links",
		"@storybook/addon-viewport",
		"@storybook/addon-knobs",
		{ name: "@storybook/addon-docs", options: { configureJSX: true } },
		"@storybook/addon-a11y",
		"@storybook/addon-storysource",
	],
	webpackFinal: async (config) => {
		config.module.rules.push({
			test: /\.(ts|tsx)$/,
			use: [
				{
					loader: require.resolve("react-docgen-typescript-loader"),
					options: {
						shouldExtractLiteralValuesFromEnum: true,
						propFilter: (prop) => {
							if (prop.parent) {
								return !prop.parent.fileName.includes(
									"node_modules"
								);
							}
							return true;
						},
					},
				},
			],
		});
		config.resolve.extensions.push(".ts", ".tsx");
		return config;
	},
};