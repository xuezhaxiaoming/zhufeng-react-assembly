/*
 * @Description: 
 * @Version: 
 * @Author: 学渣小明
 * @Date: 2020-08-15 11:22:42
 * @LastEditors: 学渣小明
 * @LastEditTime: 2020-08-15 21:10:21
 */

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}
import React from "react";
import { GlobalStyle } from "../src/components/shared/global";
import { addDecorator, addParameters } from "@storybook/react";
import { withA11y } from "@storybook/addon-a11y";

addParameters({
	options: {
		showRoots: true,
	},
	dependencies: {
		withStoriesOnly: true,
		hideEmpty: true,
	},
});
addDecorator(withA11y);
addDecorator((story) => (
	<>
		<GlobalStyle />
		{story()}
	</>
));